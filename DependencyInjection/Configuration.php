<?php

namespace VideoTargetBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        if (is_callable(TreeBuilder::class, 'getRootNode')) {
            // for symfony/config 5.0 and newer
            $treeBuilder = new TreeBuilder('video_target');
            $rootNode = $treeBuilder->getRootNode();
        } else {
            // for symfony/config 4.1 and older
            $treeBuilder = new TreeBuilder();
            $rootNode = $treeBuilder->root('video_target');
        }

        $rootNode
            ->children()
                ->scalarNode('uri')->isRequired()->end()
                ->arrayNode('auth')
                    ->children()
                        ->scalarNode('username')->isRequired()->end()
                        ->scalarNode('password')->isRequired()->end()
                    ->end()
                ->end() // auth
                ->integerNode('cache_lifetime')->defaultValue(3600)->end()
                ->scalarNode('cache_directory')->defaultValue('%kernel.cache_dir%/vt')->end()
            ->end()
        ;
        return $treeBuilder;
    }
}